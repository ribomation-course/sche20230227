#include <iostream>
#include <thread>
#include <syncstream>
#include "receivable.hxx"

namespace rm= ribomation::concurrent;
using std::cout;
using std::osyncstream;

struct Consumer : rm::Receivable<long> {
    void operator()() {
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << "[cons] msg:" << msg << "\n";
        }
        osyncstream{cout} << "[cons] done\n";
    }
};

struct Transformer : rm::Receivable<long> {
    rm::Receivable<long>* next = nullptr;
    explicit Transformer(rm::Receivable<long>* next) : next(next) {}

    void run() {
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << "    [tran] msg:" << msg << "\n";
            next->send(2 * msg);
        }
        next->send(0);
        osyncstream{cout} << "    [tran] done\n";
    }
};

struct Producer {
    unsigned const N;
    rm::Receivable<long>* next = nullptr;
    Producer(unsigned n, rm::Receivable<long>* next) : N(n), next(next) {}

    void operator ()() const {
        for (auto msg = 1U; msg <= N; ++msg) {
            osyncstream{cout} << "[prod] msg:" << msg << "\n";
            next->send(msg);
        }
        next->send(0);
    }
};

int main() {

}
