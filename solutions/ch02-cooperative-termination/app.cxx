#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
#include <vector>
#include <chrono>

namespace cr = std::chrono;
using namespace std::chrono_literals;
using std::string;
using std::cout;
using std::osyncstream;

int main() {
    auto T = std::thread::hardware_concurrency();
    auto body = [](std::stop_token st, unsigned id) {
        auto tab = string((id - 1) * 5, ' ');
        do {
            osyncstream{cout} << tab
                              << "[thr-" << id << "] hello\n";
            std::this_thread::sleep_for(.5s);
            if (st.stop_requested()) {
                osyncstream{cout} << "[thr-" << id << "] done\n";
                return;
            }
        } while (true);
    };
    cout << "[main] enter\n";
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) {
            threads.emplace_back(body, id);
        }
        cout << "[main] before sleep\n";
        std::this_thread::sleep_for(10s);
        cout << "[main] before stop\n";
        for (auto&& t: threads) t.request_stop();
        for (auto&& t: threads) t.join();
    }
    cout << "[main] exit\n";
}
