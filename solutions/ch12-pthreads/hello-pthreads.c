#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct {
    unsigned id;
    unsigned n;
} Param;

typedef struct {
    unsigned id;
    unsigned value;
} Reply;

void* thread_body(void* arg) {
    Param* param = (Param*) arg;
    printf("[thread-%d] started\n", param->id);
    for (unsigned k = 0; k < param->n; ++k) {
        printf("[thread-%d] loop %d\n", param->id, k + 1);
        usleep(10 * 1000);
    }
    printf("[thread-%d] done\n", param->id);
    Reply* reply = (Reply*) calloc(1, sizeof(Reply));
    reply->id = param->id;
    reply->value = param->id * 42;
    return (void*) reply;
}

int main() {
    unsigned T = 10U;
    unsigned N = 100U;
    pthread_t threads[T];
    Param params[T];

    printf("[main] before create\n");
    for (unsigned k = 0; k < T; ++k) {
        params[k].id = k + 1;
        params[k].n = N;
        pthread_create(&threads[k], NULL, &thread_body, &params[k]);
    }
    printf("[main] before join\n");
    for (unsigned k = 0; k < T; ++k) {
        Reply* reply;
        pthread_join(threads[k], (void**) &reply);
        printf("[main] reply: %d, %d\n", reply->id, reply->value);
        free(reply);
    }
    printf("[main] done\n");
}
