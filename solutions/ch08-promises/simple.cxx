#include <iostream>
#include <chrono>
#include <future>

namespace cr = std::chrono;
using namespace std::chrono_literals;
using std::cout;

int main() {
    auto result = std::async([] {
        std::this_thread::sleep_for(2s);
        return 42;
    });

    cout << "[main] enter\n";
    auto start = cr::high_resolution_clock::now();
    cout << "[main] result=" << result.get() << "\n";
    auto end = cr::high_resolution_clock::now();
    cout << "[main] elapsed " << cr::duration_cast<cr::milliseconds>(end-start).count() << " ms\n";
}
