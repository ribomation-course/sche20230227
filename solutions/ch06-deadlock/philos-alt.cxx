#include <iostream>
#include <syncstream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;
using std::cout;
using std::osyncstream;

int main() {
    auto philo = [](unsigned id, std::mutex* left, std::mutex* right) {
        auto const tab = std::string(5 * (id - 1), ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] hungry\n";
            {
                auto g = std::scoped_lock{*left, *right};
                osyncstream{cout} << tab << "[" << id << "] EATING\n";
                std::this_thread::sleep_for(.1s);
            }
            osyncstream{cout} << tab << "[" << id << "] thinking\n";
            std::this_thread::sleep_for(.5s);
        } while (true);
    };

    auto const P = 5U;
    auto chopsticks = std::vector<std::mutex>(P);
    auto philos = std::vector<std::jthread>{};
    for (auto k = 0U; k < P; ++k) {
        auto l = k;
        auto r = (k + 1) % P;
        philos.emplace_back(philo, k + 1, &chopsticks[l], &chopsticks[r]);
    }
}

