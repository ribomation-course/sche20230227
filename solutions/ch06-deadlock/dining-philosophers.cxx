#include <iostream>
#include <thread>
#include <vector>
#include "chopstick.hxx"
#include "philosopher.hxx"

using std::cout;
using std::osyncstream;
namespace rm = ribomation::concurrent;

int main() {
    auto no_deadlock = true;
    auto const P = 5U;
    auto chopsticks = std::vector<rm::Chopstick>(P);
    auto philosophers = std::vector<std::jthread>{};
    for (auto k = 0U; k < P; ++k) {
        auto rightIdx = k;
        auto leftIdx = (k + 1) % P;
        if (no_deadlock && k == 0) std::swap(rightIdx, leftIdx);
        philosophers.emplace_back(rm::Philosopher{k + 1, chopsticks[rightIdx], chopsticks[leftIdx]});
    }
}
