#pragma once

#include <iostream>
#include <string>
#include <syncstream>
#include "chopstick.hxx"

namespace ribomation::concurrent {
    using std::string;
    using std::cout;
    using std::osyncstream;
    using namespace std::chrono_literals;

    class Philosopher {
        unsigned const id;
        Chopstick& right;
        Chopstick& left;
    public:
        Philosopher(unsigned id_, Chopstick& right_, Chopstick& left_)
                : id{id_}, right{right_}, left{left_} {}

        [[noreturn]] void operator ()() {
            auto const tab = string(5 * (id - 1), ' ');
            do {
                osyncstream{cout} << tab << "[" << id << "] want right chopstick\n";
                right.acquire();

                osyncstream{cout} << tab << "[" << id << "] want left chopstick\n";
                left.acquire();

                osyncstream{cout} << tab << "[" << id << "] EATING\n";
                std::this_thread::sleep_for(.5s);

                osyncstream{cout} << tab << "[" << id << "] releasing both chopsticks\n";
                left.release();
                right.release();
                //std::this_thread::sleep_for(2s);
            } while (true);
        }
    };

}

