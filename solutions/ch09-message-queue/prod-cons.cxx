#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue-bounded.hxx"
using std::cout;
using std::osyncstream;
namespace rm = ribomation::concurrent;

int main() {
    auto const N = 1'000UL;
    auto q = rm::MessageQueueBounded<unsigned long>{};
    {
        auto consumer = std::jthread{[&q] {
            for (auto msg = q.get(); msg != 0; msg = q.get())
                osyncstream{cout} << "[cons] " << msg << "\n";
            cout << "[consumer] done\n";
        }};
        {
            auto odd = std::jthread{[&q] {
                for (auto k = 1UL; k <= N; k += 2) q.put(k);
                cout << "[odd] done\n";
            }};
            auto even = std::jthread{[&q] {
                for (auto k = 2UL; k <= N; k += 2) q.put(k);
                cout << "[even] done\n";
            }};
        }
        cout << "[main] before put(0)\n";
        q.put(0);
    }
    cout << "[main] exit\n";
}
