#pragma once
#include <functional>
#include <pthread.h>
#include "mutex.hxx"

namespace ribomation::concurrent {

    class Condition {
        mutable pthread_cond_t condition{};
        Mutex& mutex;
    public:
        explicit Condition(Mutex& mutex) : mutex(mutex) {
            pthread_condattr_t attrs{};
            pthread_condattr_init(&attrs);
            pthread_condattr_setpshared(&attrs, PTHREAD_PROCESS_SHARED);
            pthread_cond_init(&condition, &attrs);
            pthread_condattr_destroy(&attrs);
        }
        ~Condition() { pthread_cond_destroy(&condition); }

        void wait(std::function<bool()> const& ok_to_proceed) {
            while (!ok_to_proceed()) {
                pthread_cond_wait(&condition, mutex.native());
            }
        }
        void notify() { pthread_cond_signal(&condition); }
        void notifyAll() { pthread_cond_broadcast(&condition); }
    };

}


