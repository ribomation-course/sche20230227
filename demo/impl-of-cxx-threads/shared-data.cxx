#include <iostream>
#include <unistd.h>
#include "thread.hxx"
#include "mutex.hxx"

using std::cout;
namespace rm = ribomation::concurrent;

struct SharedData {
    rm::Mutex exclusive{};
    int amount{};
    int increment() {
        auto g = rm::LockGuard{exclusive};
        return ++amount;
    }
};

int main() {
    cout << "[main] enter\n";
    auto data = SharedData{};
    {
        auto t1 = rm::Thread{[&data] {
            cout << "[thread-1] started\n";
            for (auto k = 0; k < 100; ++k) {
                data.increment();
                usleep(100);
            }
            cout << "[thread-1] done\n";
        }};
        auto t2 = rm::Thread{[&data] {
            cout << "[thread-2] started\n";
            for (auto k = 0; k < 100; ++k) {
                data.increment();
                usleep(200);
            }
            cout << "[thread-2] done\n";
        }};
        auto t3 = rm::Thread{[&data] {
            cout << "[thread-3] started\n";
            for (auto k = 0; k < 100; ++k) {
                data.increment();
                usleep(300);
            }
            cout << "[thread-3] done\n";
        }};
    }
    cout << "[main] data: " << data.amount << "\n";
}


