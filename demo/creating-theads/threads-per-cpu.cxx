#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <syncstream>
using std::cout;

int main() {
    auto body = [](unsigned id, unsigned n) {
        auto const tab = std::string((id - 1) * 5, ' ');
        do {
            std::osyncstream{cout} << tab << "[t-" << id << "] "
                                   << "message #" << n << "\n";
        } while (--n > 0);
    };
    auto const T = std::jthread::hardware_concurrency();
    cout << "[main] launching " << T << " threads\n";
    {
        auto const N = 10;
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto k = 1U; k <= T; ++k) threads.emplace_back(body, k, N);
    }
    cout << "[main] exit\n";
}

