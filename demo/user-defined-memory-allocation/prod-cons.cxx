#include <iostream>
#include "message-thread.hxx"
namespace rm = ribomation::concurrent;
using std::cout;

struct Consumer : rm::MessageThread<long> {
protected:
    void run() override {
        cout << "    [cons] started\n";
        for (long* msg = recv(); *msg != -1; msg = recv()) {
            cout << "    [cons] " << *msg << "\n";
            dispose(msg);
        }
        cout << "    [cons] done\n";
    }
};

int main() {
    auto c = Consumer{};
    c.start();
    cout << "[prod] started\n";
    for (auto msg = 1; msg <= 1000; ++msg) c.send(msg);
    c.send(-1);
    cout << "[prod] done\n";
}


