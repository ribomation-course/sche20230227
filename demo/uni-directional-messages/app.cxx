#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue-bounded.hxx"

using ribomation::concurrent::MessageQueueBounded;
using std::cout;
using std::osyncstream;

template<typename MessageType>
struct Receivable {
private:
    MessageQueueBounded<MessageType> inbox;
protected:
    MessageType recv() { return inbox.get(); }
public:
    void send(MessageType msg) { inbox.put(msg); }

    Receivable() = default;
    virtual ~Receivable() = default;
    Receivable(Receivable const&) = delete;
    auto operator =(Receivable const&) -> Receivable& = delete;
};

struct Consumer : Receivable<long> {
    Consumer() = default;
    void run() {
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << "        [cons] msg:" << msg << "\n";
        }
        osyncstream{cout} << "        [cons] done\n";
    }
};

struct Transformer : Receivable<long> {
    Receivable<long>* next = nullptr;
    explicit Transformer(Receivable<long>* next) : next(next) {}

    void run() {
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << "    [tran] msg:" << msg << "\n";
            next->send(2 * msg);
        }
        next->send(0);
        osyncstream{cout} << "    [tran] done\n";
    }
};

struct Producer {
    unsigned const N = 100;
    Receivable<long>* next = nullptr;
    Producer(unsigned n, Receivable<long>* next) : N(n), next(next) {}

    void operator ()() const {
        for (auto msg = 1U; msg <= N; ++msg) {
            osyncstream{cout} << "[prod] msg:" << msg << "\n";
            next->send(msg);
        }
        next->send(0);
    }
};

int main() {
    auto cc = Consumer{};
    auto c = std::jthread{&Consumer::run, &cc};
    auto tt = Transformer{&cc};
    auto t = std::jthread{&Transformer::run, &tt};
    auto p = std::jthread{Producer{100, &tt}};
}
