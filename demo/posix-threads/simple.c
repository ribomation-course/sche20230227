#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* body(void* args) {
    unsigned id = (unsigned)args;
    printf("[thr-%d] started\n", id);
    sleep(1);
    printf("[thr-%d] done\n", id);
    return NULL;
}

int main() {
    printf("[main] enter\n");
    unsigned N = 5;
    pthread_t threads[N];
    for (unsigned k = 0; k < N; ++k)
        pthread_create(&threads[k], NULL, body, (void*) (k + 1));

    printf("[main] before join\n");
    for (unsigned k = 0; k < N; ++k) pthread_join(threads[k], NULL);

    printf("[main] exit\n");
}

