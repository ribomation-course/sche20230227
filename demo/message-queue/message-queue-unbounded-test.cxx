#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue-unbounded.hxx"

using ribomation::concurrent::MessageQueueUnbounded;
using std::cout;

int main() {
    auto N = 100'000L;
    auto q = MessageQueueUnbounded<long>{};

    auto consumer = std::jthread{[&q] {
        auto sum = 0L, count = 0L;
        for (auto msg = q.get(); msg > 0; msg = q.get()) {
            sum += msg;
            ++count;
        }
        cout << "[consumer] count=" << count << ", sum=" << sum << "\n";
    }};

    for (auto msg = 1; msg <= N; ++msg) q.put(msg);
    q.put(0);
    cout << "[producer] sum=" << (N * (N + 1) / 2) << "\n";
}

