cmake_minimum_required(VERSION 3.22)
project(cooperative_interruption)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(implicit-stop implicit-stop.cxx)
target_compile_options(implicit-stop PRIVATE ${WARN})

add_executable(explicit-stop explicit-stop.cxx)
target_compile_options(explicit-stop PRIVATE ${WARN})

add_executable(stop-callback stop-callback.cxx)
target_compile_options(stop-callback PRIVATE ${WARN})


